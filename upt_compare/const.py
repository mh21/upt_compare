"""Constants for upt-compare project."""
import re

# id of the merge request on upt that is being used to capture the discussion and results.
UPT_TESTING_MR_ID = 50

# How many pipelines to run at once.
UPT_COMPARE_PARALEL_RUN_COUNT = 2

# Where is upt project on gitlab.
UPT_GITLAB_PROJECT = 'cki-project/upt'
# Where is pipeline project on gitlab.
CKI_PIPELINE_GITLAB_PROJECT = 'cki-project/cki-pipeline'

# A regex to capture cki-ci-bot testing command this project uses.
RGX_BOT_TRIGGER_COMMENT = '^@cki-ci-bot \\[test\\]\\[.*?/{pipeline_id}\\].*$'

# A regex to capture cki-ci-bot testing results.
# Example data: "| cki | upstream-stable | [123456](https://<redacted>/123456)) | :exclamation: failed |"
BOT_COMMENT_RGX = re.compile(r'^\| ([^\n\\| ]+) \| ([^\n\\| ]+) \| \[([0-9]+)\\](?:[^\n\\| ]+) \| ([^\n\\| ]+) |$')

# These are the names of allowed states of results for pipeline comparison.
UPT_COMPARE_RESULTS = ('MANUAL:OK', 'MANUAL:SKT_FAILED', 'MANUAL:UPT_FAILED')
