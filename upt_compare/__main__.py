"""Main upt-compare module, cli entry point."""
import click

from upt_compare.compare import UptCompare
from upt_compare import const as compare_const

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


# pylint: disable=no-value-for-parameter

@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
def cli(ctx, **kwargs):
    """Click cli entry point."""
    print('UPT-compare CLI started')
    ctx.ensure_object(dict)


@cli.command()
@click.option('-d', '--delay', required=False, type=int, default=180,
              help='How many seconds to wait before checking results on gitlab again. Default: 180.')
@click.pass_context
def run(ctx, delay):
    """Do all; load new data from mailing list, update db-note in testing PR, run CI tests and compare results."""
    print('Doing CI run (all)...')
    upt_compare = UptCompare()
    upt_compare.start(delay)


@cli.command()
@click.pass_context
def test(ctx):
    """Load new data from mailing list, update db-note in testing PR, run CI tests."""
    print('Running CI tests...')
    upt_compare = UptCompare()
    db_note = upt_compare.stage1_update_info(compare_const.UPT_GITLAB_PROJECT, compare_const.UPT_TESTING_MR_ID)
    # If we're not already running more pipelines than intended, move some pipelines from to-do to running.
    upt_compare.stage2_run_tests(db_note)


@cli.command()
@click.option('--create', required=False, type=bool, default=False, is_flag=True,
              help='When used, create a db-note in the testing PR.')
@click.pass_context
def updatedb(ctx, create):
    """Load pipeline ids from mailing list and update the db-note in testing PR."""
    print('Updating db-note from mailing list...')
    upt_compare = UptCompare()
    upt_compare.stage1_update_info(compare_const.UPT_GITLAB_PROJECT, compare_const.UPT_TESTING_MR_ID, create)


@cli.command()
@click.option('-p', '--pipeline-id', required=True, type=int, multiple=True,
              help='A pipeline_id of an original skt pipeline that was retriggered using cki-ci-bot for upt.'
                   'Multiple values are possible by repeating this option.')
@click.pass_context
def getresult(ctx, pipeline_id):
    """Get results for an original pipeline_id (with skt) that we tested again using upt."""
    print(f'Getting testing result for pipeline_id(s): {list(pipeline_id)}')

    upt_compare = UptCompare()
    # Get all discussion objects of the testing PR.
    discussions = upt_compare.comments_helper.get_discussions()
    for pid in pipeline_id:
        for result in upt_compare.comments_helper.get_result_for_pipeline(discussions, pid):
            print(str(result))


@cli.command()
@click.option('-p', '--pipeline-id', required=True, type=str, multiple=True,
              help='A pipeline_id of an original skt pipeline that was retriggered using cki-ci-bot for upt.'
                   'The related comment about will be removed')
@click.option('--restore/--no-restore', required=False, default=True,
              help='If True, move the pipeline back to TODO in db note. Default: True.')
@click.pass_context
def delete(ctx, pipeline_id, restore):
    """Remove broken testing results from upt testing PR."""
    print(f'Deleting results ({list(pipeline_id)}) from upt testing PR...')
    upt_compare = UptCompare()
    upt_compare.remove_broken_comments(pipeline_id, restore)


@cli.command()
@click.option('-p', '--pipeline-id', required=True, type=str, multiple=True,
              help='A pipeline_id of an original skt pipeline that was retriggered using cki-ci-bot for upt.'
                   '')
@click.option('-r', '--result', required=True, type=str,
              help='Mark a discussion of skt/upt comparison with a final result. Allowed values: '
              + ', '.join(compare_const.UPT_COMPARE_RESULTS))
@click.option('--set-done/--no-set-done', required=False, default=True,
              help='If True, move the pipeline ids specified by -p to "Done" in MR. Default: True.')
@click.pass_context
def mark(ctx, pipeline_id, result, set_done):
    """Mark a testing pipeline with upt-compare result, indicating whether UPT and SKT work in the same way."""
    print(f'Marking pipeline results ({list(pipeline_id)}) as {result}...')

    if ':' not in result or ',' not in result or result.split(',')[0] not in \
            compare_const.UPT_COMPARE_RESULTS:
        raise RuntimeError(f'Invalid value "{result}", use one of {", ".join(compare_const.UPT_COMPARE_RESULTS)},msg.')
    else:
        upt_compare = UptCompare()
        upt_compare.mark_results(pipeline_id, result, set_done)


@cli.command()
@click.option('--finalize/--no-finalize', required=False, default=False,
              help='If True, move finished from running to done and update db note.')
@click.pass_context
def finished(ctx, finalize):
    """Get results that are running according to the note, but have just finished."""
    print('Getting results from upt testing PR...')
    upt_compare = UptCompare()

    # Get db-note so we know what results are
    db_note = upt_compare.comments_helper.get_db_note()
    for lst in upt_compare.find_finished_results(db_note):
        for result in lst:
            if result.branch in db_note.running and finalize:
                print(f'Moving "{str(result)}" to done...')

                db_note.done.add(result.branch)
                db_note.running.remove(result.branch)
            else:
                print(result)

    if finalize:
        # Write changes
        db_note.update()


def main():
    """Start upt-compare cli."""
    cli()


if __name__ == '__main__':
    main()
